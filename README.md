# jquery.shikari.js - Плагин jQuery для загрузки объявлений с shikari.do

## Назначение 

Плагин предназначен для загрузки триал-объявлений с сайта [shikari.do](https://shikari.do)

Триал-объявления - это реальные объявления, найденные сервисом, но срок которых уже несколько дней (обычно - около недели). 

Плагин позволяет загружать эти объявления на страницу любого стороннего сайта, например, 
для привлечения посетителей по [партнерской программе shikari.do](https://shikari.do/partner-terms)   

Вы можете просто добавить на свой сайт объявления той категории, которая лучше всего соответствует
тематике сайта, чтобы повысить его привлекательность и полезность для посетителей, а можете
зарегистрироваться в партнерской программе сервиса [shikari.do](https://shikari.do/partner-terms) и 
получать партнеское вознаграждение.

## Возможности

* Загрузка объявлений из любой категории (или из нескольких категорий) и вставка их в заданный элемент на странице
* Переход из отображаемых объявлений по своей партнерской ссылке
* Задание шаблона для отображения объявлений
* Стиллизация отображаемых объявлений

## Быстрый запуск для ленивых

Если вам лень читать, что и как настраивается, то можете сделать очень просто:

1. Убедитесь, что у вас подключена библиотека **jQuery** или подключите ее ДО использования плагина (если не знаете, как это сделать, см. ниже в подробностях)

2. Добавьте в нужное место на сайте блочный элемент, куда будут загружаться объявления:
```html
<div id="shikari-do"></div>
```

3. Вставьте перед закрывающим тегом `</body>`
```html
<script src="//shikari.do/widget/dist/jquery.shikari.min.js"></script>
<script>
$(function () {
    $("#shikari-do").shikari(
        1234, { // здесь указываете свой партнерский ID
            category: [1],      // здесь указываете ID категорий 
            loadStyles: true    // загрузка стилей по умолчанию
        });
});
</script>
```

## Использование (подробности)

Подключите jQuery, например, так:

```html
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
```

Подключите стили и код плагина:

```html
<link rel="stylesheet" href="dist/jquery.shikari.min.css" type="text/css">
<script src="dist/jquery.shikari.min.js"></script>
```
> Важно: плагин должен подключаться после библиотеки jQuery 


Вызовите исполнение плагина, привязав его к элементу, куда должны загружаться объявления, при вызове укажите ваш партнерский ID (в примере ниже это число 1234, замените его на свой ID):

```javascript
$("#shikari").shikari(1234);
```

## Параметры

При вызове плагина ему можно передать дополнительные параметры:

```javascript
$("#shikari").shikari(1234, {
    category: [4, 8],
    limit: 5
});
```

Описание параметров дано ниже

#### category: int | array

Список ID категорий, из которых нужно получать объявления. 

Список категорий и их ID:

* 1 - Фото и видеосъемка
* 6 - WEB/IT-специалисты
* 7 - Ремонт и обслуживание техники
* 9 - Организация праздников
* 10 - Творческие услуги и дизайн
* 13 - Путешествия
* 14 - Обучение, репетиторство, курсы
* 16 - Юридические услуги
* 17 - Бухгалтерские услуги
* 19 - Поиск домашнего персонала
* 21 - Стройка и бытовой ремонт
* 22 - Медицинские услуги
* 26 - Авторы, писатели, журналисты
* 27 - Стиль и красота
* 33 - Аренда недвижимости
* 34 - Интернет-маркетинг
* 37 - Музыка
* 38 - Переводчики и ин.языки
* 39 - Мебель и кухни

Регулярно добавляются новые категории, и полный список работающих категорий вы всегда можете посмотреть на странице [shikari.do/partner](https://shikari.do/partner)
(см. на странице ссылку "список актуальных категорий") 

#### limit: int

Максимальное число объявлений, которые вы хотите отобразить

#### templateUrl: string

Ссылка на собственный загружаемый шаблон

#### template: string

Код шаблона для отображения объявлений 
> Если задан параметр templateUrl, то код загружается по заданной ссылке

#### beforeRender: function(items, template)

Функция, которая вызывается после того, как данные получены, но перед тем, как объявления будут отображены. 
В функцию передаются два аргумента: массив полученных объявлений в формате json и шаблон (если был задан параметр templateUrl, 
то передается уже загруженный шаблон).
   
Если функция возвращает значение `false`, то вывод объявлений не выполняется.

#### autoSize: bool

Автоматическая подгонка размеров содержимого блока объявлений к размерам элемента-контейнера. По умолчанию - `true`.

## Шаблонизация и стиллизация

Шаблон для выводя объявлений, используемый по умолчанию, выглядит так:

```html
<div class="shikari-do-request">
    <div class="shikari-do-request-ava">
        <svg class="shikari-do-request-ava-svg">
            <image class="shikari-do-request-ava-image" style="clip-path: url('#shikari-do-svg-clip-hexagon')"
                   xmlns:xlink="https://www.w3.org/1999/xlink" xlink:href="{{user_ava}}"></image>
        </svg>
    </div>
    <div class="shikari-do-request-info">
        <div class="shikari-do-request-info-snippet">{{snippet}}</div>
        <div class="shikari-do-request-info-meta"><span class="shikari-do-request-info-meta-date">{{datetime}}</span>
            <img src="{{resourse_fav}}" class="shikari-do-request-info-meta-favicon"><span
                    class="shikari-do-request-info-meta-resourse">{{resourse_name}}</span></div>
    </div>
</div>
```

Используемый файл стилей по умолчанию, соответственно, задает стили для CSS-классов с префиксом `shikari-do`, 
но вы, при желании, можете создать собственный файл стилей, чтобы оформить загружаемые объявления 
в соответствии со стиллистикой свойго сайта.

## License

[MIT License](https://opensource.org/licenses/MIT) © Shikari.Do Team
